﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(ConstantStock.Web.Startup))]

namespace ConstantStock.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
