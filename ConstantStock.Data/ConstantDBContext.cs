﻿using ConstantStock.Domain;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ConstantStock.Data
{
    public class ConstantDBContext : IdentityDbContext<ApplicationUser>
    {
        public ConstantDBContext()
            : base("DefaultConnection")
        {
        }
        public static ConstantDBContext Create()
        {
            return new ConstantDBContext();
        }
    }
}
