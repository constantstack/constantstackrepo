﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace ConstantStock.Domain
{
    public class ApplicationUser : IdentityUser
    {
    }
}
